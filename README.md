# kubernetes-gosample 
 this application is related to kubernetes with golang sample

```
git remote add pfm git@gitlab.com:pfm3/mail/kubernetes-gosample.git
git checkout -b feature-1
vim README.md
git add .
git commit -m "ADD feature-1"
git push pfm feature-1

```

```
git remote -v
origin	git@gitlab.com:azalio/kubernetes-gosample.git (fetch)
origin	git@gitlab.com:azalio/kubernetes-gosample.git (push)
pfm	git@gitlab.com:pfm3/mail/kubernetes-gosample.git (fetch)
pfm	git@gitlab.com:pfm3/mail/kubernetes-gosample.git (push)
```

```
docker build -t registry.gitlab.com/pfm3/mail/kubernetes-gosample:feature-1 .
docker push registry.gitlab.com/pfm3/mail/kubernetes-gosample:feature-1
```

```
kubectl create ns mail-feature-1
kubectl apply -f config/deploy.yaml -n mail-feature-1
kubectl get pod -n mail-feature-1
NAME                           READY   STATUS    RESTARTS   AGE
hello-world-54d44bf6db-8b467   1/1     Running   0          37m
```
